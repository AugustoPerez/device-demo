package myapp.action;

import myapp.entity.Device;
import myapp.service.DeviceService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/devices")
public class Devices {

    @Inject
    private DeviceService deviceService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Device> findAll() {
        return new ArrayList<>(deviceService.findAll());
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Device find(@PathParam("id") long id) {
        return deviceService.findById(id);
    }

    @GET
    @Path("/mac/{mac}")
    @Produces(MediaType.APPLICATION_JSON)
    public Device findByMac(@PathParam("mac") String mac) {
        return deviceService.findByMac(mac);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDevice(Device device) {
        try {
            device = deviceService.create(device);
            return Response.ok(device).build();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

}