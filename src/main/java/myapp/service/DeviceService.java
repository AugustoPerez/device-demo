package myapp.service;

import myapp.entity.Device;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static myapp.OfyService.ofy;

public class DeviceService {

    public List<Device> findAll() {
        return ofy().load().type(Device.class).list();
    }

    public Device findById(Long id) {
        return ofy().load().type(Device.class).id(id).now();
    }

    public Device create(Device device) throws Exception {
        if (timestampValid(device.getTimestamp()) && macValid(device.getMac()) && findByMac(device.getMac()) == null) {
            ofy().save().entity(device).now();
            return findById(device.getId());
        } else {
            throw new Exception("Invalid data");
        }
    }

    public Device findByMac(String mac) {
        return ofy().load().type(Device.class).filter("mac", mac).first().now();
    }

    private boolean timestampValid(Date timestamp) {
        try {
            String pattern = "dd-MM-yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            Date date = simpleDateFormat.parse("01-01-2018");
            return timestamp.compareTo(date) > -1;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean macValid(String mac) {
        Pattern p = Pattern.compile("[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}");
        Matcher m = p.matcher(mac);
        return m.matches();
    }

}