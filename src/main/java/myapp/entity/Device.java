package myapp.entity;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Device implements Serializable{

    @Id
    private Long id;
    @Index
    private String mac;
    private Date timestamp;

    public static Key<Device> key(long id) {
        return Key.create(Device.class, id);
    }

    public Device() {}

    public Device(String mac, Date timestamp) {
        this.mac = mac;
        this.timestamp = timestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}