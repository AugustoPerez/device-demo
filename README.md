# Devices Demo

Devices Demo is an API that serves endpoint to:
* GET: https://proyecto-weebee.appspot.com/api/devices : get all registered devices
* GET: https://proyecto-weebee.appspot.com/api/devices/{id} : get a device by its id
* GET: https://proyecto-weebee.appspot.com/api/devices : get a device by its id mac
* POST: https://proyecto-weebee.appspot.com/api/devices : register a new device


## Technology

Devices Demo is built on:

* [Google App Engine](http://code.google.com/appengine/) (for Java)
* [Objectify-Appengine](http://code.google.com/p/objectify-appengine/)
* [Guice](http://code.google.com/p/google-guice/)
* [Jersey](http://jersey.java.net/)

### Development

* Create a App Engine application 
* Install the Google Cloud SDK 
* Install the Google App Engine SDK
* Open the Devices Demo project, import the pom.xml
* Edit war/WEB-INF/appengine-web.xml and replace *proyecto-weebee* with your application id
